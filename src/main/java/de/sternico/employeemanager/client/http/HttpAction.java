package de.sternico.employeemanager.client.http;

public enum HttpAction
{
    GET("GET"), DELETE("DELETE"), POST("POST"), PUT("PUT");

    private final String action;

    private HttpAction(String aAction)
    {
        this.action = aAction;
    }
    
    public String getAction()
    {
        return action;
    }
}