package de.sternico.employeemanager.client.http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestClient<T>
{
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    public List<T> execute(String aUrl, HttpAction aHttpAtion, Class<?> resultClazz, Object aRequestBodyObject)
            throws Exception
    {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpUriRequest request = getHttpUriRequest(aHttpAtion, aUrl, AcceptedType.JSON);
        if (aRequestBodyObject != null)
        {
            if (request instanceof HttpEntityEnclosingRequestBase && aRequestBodyObject != null)
            {
                String aBody = JSON_MAPPER.writeValueAsString(aRequestBodyObject);
                ((HttpEntityEnclosingRequestBase) request).setEntity(new StringEntity(aBody));   
            }            
        }
        CloseableHttpResponse response = client.execute(request);
        InputStream is = response.getEntity().getContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null)
        {
            sb.append(line).append('\n');
        }
        List<T> items = JSON_MAPPER.readerForListOf(resultClazz).readValue(sb.toString().getBytes());
        List<T> result = new ArrayList<>();
        for (T item : items)
        {
            result.add(item);
        }
        IOUtils.closeQuietly(response);
        return result;        
    }
    
    private static HttpUriRequest getHttpUriRequest(HttpAction action, String uri, AcceptedType type)
    {
        HttpUriRequest request = null;
        switch (action)
        {
            case DELETE:
                request = new HttpDelete(uri);
                break;
            case GET:
                request = new HttpGet(uri);
                break;
            case POST:
                request = new HttpPost(uri);
                break;
            case PUT:
                request = new HttpPut(uri);
                break;
        }
        request.setHeader("Accept", type.getAcceptedType());
        request.setHeader("Content-type", "application/json");
        return request;
    }
    
    public enum AcceptedType
    {
        JSON("application/json"),
        XML("application/xml");

        private String type;

        private AcceptedType(String type)
        {
            this.type = type;
        }

        String getAcceptedType()
        {
            return type;
        }
    }
}