package de.sternico.employeemanager.client.view;

import javax.swing.JFrame;

import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Employee;

public abstract class JFrameBasedEmployeeView extends JFrame implements IEmployeeView
{
    private static final long serialVersionUID = 9066737734587684369L;
    
    @Override
    public void showView()
    {
        setVisible(true);
    }
    
    protected abstract void displayEmployeeDetails(Employee aEmployee);
    
    protected abstract void changeCompanyName(Company aCompany, String companychange);
}