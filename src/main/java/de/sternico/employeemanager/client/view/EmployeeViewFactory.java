package de.sternico.employeemanager.client.view;

import java.util.HashMap;

import de.sternico.employeemanager.client.ApplicationIdentifier;

public class EmployeeViewFactory
{
    private static final HashMap<ApplicationIdentifier, IEmployeeView> views =
            new HashMap<ApplicationIdentifier, IEmployeeView>();
    static
    {
        views.put(ApplicationIdentifier.SPRING, new EmployeeManagerGui());
    }

    public static IEmployeeView getView(ApplicationIdentifier aIdentifier)
    {
        IEmployeeView iEmployeeView = views.get(aIdentifier);
        return iEmployeeView;
    }
}