package de.sternico.employeemanager.client.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.sternico.employeemanager.client.controller.IEmployeeManagerClientController;
import de.sternico.employeemanager.client.util.ListModelGenerator;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public class EmployeeManagerMainFrame extends JFrameBasedEmployeeView
{
    private static final long serialVersionUID = 7426815801332653664L;

    private JList<Company> companyList;
    
    private JList<Department> departmentList;

    private JList<Employee> employeeList;
    
    private IEmployeeManagerClientController controller;

    private JPanel employeeDetails;

    public EmployeeManagerMainFrame()
    {
        super();
        setSize(new Dimension(800, 600));
        initGui();
        initListeners();
    }

    private void initListeners()
    {
        companyList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                controller.companySelected(companyList.getSelectedValue());
            }
        });
        employeeList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                
            }
        });
    }

    private void initGui()
    {
        setLayout(new BorderLayout());

        companyList = new JList<Company>();
        companyList.setPreferredSize(new Dimension(100, 100));
        companyList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
        
        departmentList = new JList<Department>();
        departmentList.setPreferredSize(new Dimension(100, 100));
        departmentList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);

        employeeList = new JList<Employee>();
        employeeList.setPreferredSize(new Dimension(100, 100));
        employeeList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);

        employeeDetails = new JPanel();
        employeeDetails.setPreferredSize(new Dimension(300, 100));

        add(new JScrollPane(companyList), BorderLayout.WEST);
        add(new JScrollPane(departmentList), BorderLayout.SOUTH);
        add(new JScrollPane(employeeList), BorderLayout.CENTER);

        add(new JScrollPane(employeeDetails), BorderLayout.EAST);
    }

    public void acceptCompanies(List<Company> companies)
    {
        companyList.setModel(new ListModelGenerator<Company>().generate(companies));
    }

    public void acceptDepartmens(List<Department> aDepartments)
    {
        departmentList.setModel(new ListModelGenerator<Department>().generate(aDepartments));
    }

    public void acceptEmployees(List<Employee> aEmoployee)
    {
        // TODO Auto-generated method stub
        
    }
    
    public void setController(IEmployeeManagerClientController aController)
    {
        this.controller = aController;
    }

    @Override
    protected void displayEmployeeDetails(Employee aEmployee)
    {
        // TODO Auto-generated method stub
    }

    @Override
    protected void changeCompanyName(Company aCompany, String companychange)
    {
        // TODO Auto-generated method stub
        
    }
}