package de.sternico.employeemanager.client.view;

import java.util.List;

import de.sternico.employeemanager.client.controller.IEmployeeManagerClientController;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public interface IEmployeeView
{
    void acceptCompanies(List<Company> aCompanies);

    void acceptDepartmens(List<Department> aDepartments);
    
    void acceptEmployees(List<Employee> aEmoployee);
    
    void showView();

    void setController(IEmployeeManagerClientController aController);
}