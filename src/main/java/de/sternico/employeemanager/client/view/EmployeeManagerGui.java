/*
 * Created by JFormDesigner on Mon Sep 13 15:33:21 CEST 2021
 */

package de.sternico.employeemanager.client.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.sternico.employeemanager.client.controller.IEmployeeManagerClientController;
import de.sternico.employeemanager.client.util.ListModelGenerator;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

/**
 * @author Timo
 */
@SuppressWarnings("serial")
public class EmployeeManagerGui extends JFrameBasedEmployeeView
{
    private IEmployeeManagerClientController controller;

    private JButton btnCreateCompany;

    private JButton btnDeployProcess;

    private JButton btnManageProcesses;

    private JButton btnDeleteCompany;

    private JButton btnDeleteDepartment;

    public EmployeeManagerGui()
    {
        initComponents();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnCreateCompany = new JButton("Company erzeugen");
        toolBar1.add(btnCreateCompany);

        btnDeployProcess = new JButton("Prozess deployen");
        toolBar1.add(btnDeployProcess);

        btnManageProcesses = new JButton("Prozesse managen");
        toolBar1.add(btnManageProcesses);

        btnDeleteCompany = new JButton("Delete Company");
        toolBar1.add(btnDeleteCompany);

        btnDeleteDepartment = new JButton("Delete Department");
        toolBar1.add(btnDeleteDepartment);

        initListeners();
    }

    public void initListeners()
    {

        companyList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                System.out.println(companyList.getSelectedValue());
                controller.companySelected(companyList.getSelectedValue());
                System.out.println(companyList.getSelectedValue().getName());
            }
        });

        divisionList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                controller.departmentSelected(divisionList.getSelectedValue());
            }
        });

        employeeList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                displayEmployeeDetails(employeeList.getSelectedValue());
            }
        });

        btnCreateCompany.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                controller.createCompany();
            }
        });

        btnDeployProcess.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                controller.deployProcess();
            }
        });

        btnManageProcesses.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                controller.manageProcesses();
            }
        });

        btnDeleteCompany.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Company c = companyList.getSelectedValue();
                if (c == null)
                {
                    return;
                }
                controller.deleteCompany(c);
            }
        });

        openCompanyDialogButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (companyList.getSelectedValue() != null)
                {
                    ChangeCompany.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    ChangeCompany.setVisible(true);
                    companyNameInput.setText(companyList.getSelectedValue().getName());
                }
            }
        });

        changeCompanyNameButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String newCompanyName = companyNameInput.getText();
                Company selectedCompany = companyList.getSelectedValue();
                if (newCompanyName != null)
                {
                    //controller.changeCompanyName(selectedCompany, newCompanyName);
                    ChangeCompany.setVisible(false);
                }
            }
        });

        btnDeleteDepartment.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Company selectedCompany = companyList.getSelectedValue();
                Department selectedDepartment = divisionList.getSelectedValue();
                if (selectedDepartment == null)
                {
                    return;
                }
                controller.deleteDepartment(selectedCompany, selectedDepartment);
            }
        });

        openDepartmentDialogButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (companyList.getSelectedValue() != null)
                {
                    addDepartmenttoCompany.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    addDepartmenttoCompany.setVisible(true);
                }
            }
        });

        addDepartmentButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String newDepartmentName = departmentNameInput.getText();
                Company selectedCompany = companyList.getSelectedValue();
                if (newDepartmentName != null)
                {
                    controller.addDepartment(selectedCompany, newDepartmentName);
                    addDepartmenttoCompany.setVisible(false);
                }
            }
        });
    }

    private void initComponents()
    {
        // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Timo
        toolBar1 = new JToolBar();
        Companies = new JLabel();
        Employees = new JLabel();
        EmployeeInfo = new JLabel();
        Company = new JScrollPane();
        companyList = new JList<>();
        Employee = new JScrollPane();
        employeeList = new JList<>();
        employeeName = new JLabel();
        nameinput = new JLabel();
        employeeDepartment = new JLabel();
        departmentinput = new JLabel();
        employeeCompany = new JLabel();
        companyinput = new JLabel();
        openCompanyDialogButton = new JButton();
        openDepartmentDialogButton = new JButton();
        Departments = new JLabel();
        Division = new JScrollPane();
        divisionList = new JList<>();
        ChangeCompany = new JDialog();
        companyName = new JLabel();
        companyNameInput = new JTextField();
        changeCompanyNameButton = new JButton();
        addDepartmenttoCompany = new JDialog();
        departmentName = new JLabel();
        departmentNameInput = new JTextField();
        addDepartmentButton = new JButton();

        //======== this ========
        setTitle("Employee Manager");
        var contentPane = getContentPane();
        contentPane.setLayout(new GridBagLayout());
        ((GridBagLayout)contentPane.getLayout()).columnWidths = new int[] {14, 142, 190, 306, 229, 0, 0};
        ((GridBagLayout)contentPane.getLayout()).rowHeights = new int[] {0, 43, 63, 57, 38, 54, 63, 101, 0, 39, 0, 7, 0};
        ((GridBagLayout)contentPane.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};
        ((GridBagLayout)contentPane.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

        //======== toolBar1 ========
        {
            toolBar1.setFloatable(false);
        }
        contentPane.add(toolBar1, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- Companies ----
        Companies.setText("Companies");
        Companies.setFont(new Font("Tahoma", Font.BOLD, 12));
        contentPane.add(Companies, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- Employees ----
        Employees.setText("Employees");
        Employees.setFont(new Font("Tahoma", Font.BOLD, 12));
        contentPane.add(Employees, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- EmployeeInfo ----
        EmployeeInfo.setText("Employee Info");
        EmployeeInfo.setFont(new Font("Tahoma", Font.BOLD, 12));
        contentPane.add(EmployeeInfo, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== Company ========
        {

            //---- companyList ----
            companyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            Company.setViewportView(companyList);
        }
        contentPane.add(Company, new GridBagConstraints(1, 2, 2, 6, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== Employee ========
        {

            //---- employeeList ----
            employeeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            Employee.setViewportView(employeeList);
        }
        contentPane.add(Employee, new GridBagConstraints(3, 2, 1, 6, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- employeeName ----
        employeeName.setText("Name:");
        contentPane.add(employeeName, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- nameinput ----
        nameinput.setVerticalAlignment(SwingConstants.TOP);
        contentPane.add(nameinput, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- employeeDepartment ----
        employeeDepartment.setText("Department:");
        employeeDepartment.setVerticalAlignment(SwingConstants.TOP);
        contentPane.add(employeeDepartment, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- departmentinput ----
        departmentinput.setVerticalAlignment(SwingConstants.TOP);
        contentPane.add(departmentinput, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- employeeCompany ----
        employeeCompany.setText("Company:");
        contentPane.add(employeeCompany, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- companyinput ----
        companyinput.setVerticalAlignment(SwingConstants.TOP);
        contentPane.add(companyinput, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- openCompanyDialogButton ----
        openCompanyDialogButton.setText("Change Company Name");
        contentPane.add(openCompanyDialogButton, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- openDepartmentDialogButton ----
        openDepartmentDialogButton.setText("Add Department");
        contentPane.add(openDepartmentDialogButton, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //---- Departments ----
        Departments.setText("Departments");
        Departments.setFont(new Font("Tahoma", Font.BOLD, 12));
        contentPane.add(Departments, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== Division ========
        {

            //---- divisionList ----
            divisionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            Division.setViewportView(divisionList);
        }
        contentPane.add(Division, new GridBagConstraints(1, 10, 4, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        pack();
        setLocationRelativeTo(getOwner());

        //======== ChangeCompany ========
        {
            ChangeCompany.setTitle("Add / Change Company");
            var ChangeCompanyContentPane = ChangeCompany.getContentPane();
            ChangeCompanyContentPane.setLayout(new GridBagLayout());
            ((GridBagLayout)ChangeCompanyContentPane.getLayout()).columnWidths = new int[] {131, 154, 0, 0};
            ((GridBagLayout)ChangeCompanyContentPane.getLayout()).rowHeights = new int[] {0, 41, 26, 0};
            ((GridBagLayout)ChangeCompanyContentPane.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
            ((GridBagLayout)ChangeCompanyContentPane.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

            //---- companyName ----
            companyName.setText("Company Name:");
            companyName.setHorizontalAlignment(SwingConstants.CENTER);
            ChangeCompanyContentPane.add(companyName, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            ChangeCompanyContentPane.add(companyNameInput, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

            //---- changeCompanyNameButton ----
            changeCompanyNameButton.setText("Change Company Name");
            ChangeCompanyContentPane.add(changeCompanyNameButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            ChangeCompany.pack();
            ChangeCompany.setLocationRelativeTo(ChangeCompany.getOwner());
        }

        //======== addDepartmenttoCompany ========
        {
            addDepartmenttoCompany.setTitle("Add Department");
            var addDepartmenttoCompanyContentPane = addDepartmenttoCompany.getContentPane();
            addDepartmenttoCompanyContentPane.setLayout(new GridBagLayout());
            ((GridBagLayout)addDepartmenttoCompanyContentPane.getLayout()).columnWidths = new int[] {131, 154, 0, 0};
            ((GridBagLayout)addDepartmenttoCompanyContentPane.getLayout()).rowHeights = new int[] {0, 41, 26, 0};
            ((GridBagLayout)addDepartmenttoCompanyContentPane.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
            ((GridBagLayout)addDepartmenttoCompanyContentPane.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

            //---- departmentName ----
            departmentName.setText("Department Name:");
            departmentName.setHorizontalAlignment(SwingConstants.CENTER);
            addDepartmenttoCompanyContentPane.add(departmentName, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            addDepartmenttoCompanyContentPane.add(departmentNameInput, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

            //---- addDepartmentButton ----
            addDepartmentButton.setText("Add Department");
            addDepartmenttoCompanyContentPane.add(addDepartmentButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            addDepartmenttoCompany.pack();
            addDepartmenttoCompany.setLocationRelativeTo(addDepartmenttoCompany.getOwner());
        }
        // JFormDesigner - End of component initialization //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Timo
    private JToolBar toolBar1;
    private JLabel Companies;
    private JLabel Employees;
    private JLabel EmployeeInfo;
    private JScrollPane Company;
    private JList<Company> companyList;
    private JScrollPane Employee;
    private JList<Employee> employeeList;
    private JLabel employeeName;
    private JLabel nameinput;
    private JLabel employeeDepartment;
    private JLabel departmentinput;
    private JLabel employeeCompany;
    private JLabel companyinput;
    private JButton openCompanyDialogButton;
    private JButton openDepartmentDialogButton;
    private JLabel Departments;
    private JScrollPane Division;
    private JList<Department> divisionList;
    private JDialog ChangeCompany;
    private JLabel companyName;
    private JTextField companyNameInput;
    private JButton changeCompanyNameButton;
    private JDialog addDepartmenttoCompany;
    private JLabel departmentName;
    private JTextField departmentNameInput;
    private JButton addDepartmentButton;
    // JFormDesigner - End of variables declaration //GEN-END:variables

    @Override
    public void acceptCompanies(List<Company> aCompanies)
    {
        companyList.setModel(new ListModelGenerator<Company>().generate(aCompanies));
    }

    @Override
    public void acceptDepartmens(List<Department> aDepartments)
    {
        divisionList.setModel(new ListModelGenerator<Department>().generate(aDepartments));
    }

    @Override
    public void acceptEmployees(List<Employee> aEmployee)
    {
        employeeList.setModel(new ListModelGenerator<Employee>().generate(aEmployee));
    }

    @Override
    public void setController(IEmployeeManagerClientController aController)
    {
        this.controller = aController;
    }

    @Override
    protected void displayEmployeeDetails(Employee aEmployee)
    {
        if (aEmployee != null)
        {
            nameinput.setText(aEmployee.getLastName() + " " + "," + " " + aEmployee.getFirstName());
            departmentinput.setText(aEmployee.getDepartment().getName());
            companyinput.setText(aEmployee.getDepartment().getCompany().getName());
        }
        else
        {
            nameinput.setText("");
            departmentinput.setText("");
            companyinput.setText("");
        }
    }

    @Override
    protected void changeCompanyName(Company aCompany, String companychange)
    {

        aCompany.setName(companychange);
    }
}
