package de.sternico.employeemanager.client.view.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.hibernate.Session;

import de.sternico.employeemanager.client.util.ListModelGenerator;
import de.sternico.employeemanager.client.view.IEmployeeView;
import de.sternico.employeemanager.server.entity.Company;

public class DeleteCompanyDialog extends JDialog
{
    private static final long serialVersionUID = 1874733896278592835L;

    private List<Company> taskCompanyList;

    private JButton btnConfirm;

    private JButton btnAbort;

    private JList<Company> taskList;

    private Company selectedCompany;
    
    private Session session;
    
    private Object deleteCompany;

    public DeleteCompanyDialog(IEmployeeView employeeView, List<Company> companyList)
    {
        super((Frame) employeeView);
        this.taskCompanyList = companyList;
        setModal(true);
        setSize(new Dimension(400, 300));
        initComponents();
        fillTasks();
        initListeners();
    }

    private void initListeners()
    {
        taskList.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                taskList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
                selectedCompany = taskList.getSelectedValue();
            }
        });

        btnConfirm.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (selectedCompany == null)
                {
                    return;
                }
                deleteCompanyfromDb();
            }
        });

        btnAbort.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                DeleteCompanyDialog.super.setVisible(false);
            }
        });

    }

    private void deleteCompanyfromDb()
    {
        /*
        session = EmployeeManagerSingleton.getInstance().getDatabaseSession();
        String selectedCompanyName = selectedCompany.getName();
        System.out.println(selectedCompanyName);
        System.out.println(taskCompanyList);
        
        for (Company company : taskCompanyList) {
            if (company.getName() == selectedCompanyName) {
                Long id = company.getId();
                System.out.println(id);
                Transaction tx = session.beginTransaction();
                deleteCompany = session.load(Company.class, id);
                System.out.println(deleteCompany);
                if (deleteCompany != null) {
                    session.delete(deleteCompany);
                }
                tx.commit();
                session.close();
            }
        }
        DeleteCompanyDialog.super.setVisible(false);
        */
    }

    private void fillTasks()
    {
        taskList.setModel(new ListModelGenerator<Company>().generate(taskCompanyList));
        System.out.println(taskList);
    }

    private void initComponents()
    {
        setLayout(new BorderLayout());
        taskList = new JList<Company>();
        add(new JScrollPane(taskList), BorderLayout.CENTER);
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        btnConfirm = new JButton("Best�tigen");
        toolBar.add(btnConfirm);
        btnAbort = new JButton("Abbrechen");
        toolBar.add(btnAbort);
        add(toolBar, BorderLayout.SOUTH);
        taskList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
}
