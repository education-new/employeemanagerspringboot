package de.sternico.employeemanager.client.controller;

import de.sternico.employeemanager.client.ApplicationIdentifier;
import de.sternico.employeemanager.client.exception.EmployeeManagerControllerException;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;

public interface IEmployeeManagerClientController
{
    void companySelected(Company selectedValue);

    void departmentSelected(Department selectedValue);

    void startUp(ApplicationIdentifier aIdentifier) throws EmployeeManagerControllerException;
    
    void createCompany();

    void manageProcesses();
    
    void deployProcess();

    void deleteCompany(Company aCompany);

    void addDepartment(Company selectedCompany, String newDepartmentName);

    void deleteDepartment(Company selectedCompany, Department selectedDepartment);
}