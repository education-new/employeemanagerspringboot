package de.sternico.employeemanager.client.controller;

import de.sternico.employeemanager.client.ApplicationIdentifier;
import de.sternico.employeemanager.client.exception.EmployeeManagerControllerException;
import de.sternico.employeemanager.client.model.EmployeeModelFactory;
import de.sternico.employeemanager.client.model.base.IEmployeeManagerModel;
import de.sternico.employeemanager.client.view.EmployeeViewFactory;
import de.sternico.employeemanager.client.view.IEmployeeView;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;

public class EmployeeManagerClientController implements IEmployeeManagerClientController
{
    private IEmployeeView employeeView;

    private IEmployeeManagerModel employeeModel;

    public void companySelected(Company selectedValue)
    {
        employeeView.acceptDepartmens(employeeModel.getDepartmentsByCompany(selectedValue));
    }

    public void departmentSelected(Department selectedValue)
    {
        employeeView.acceptEmployees(employeeModel.getEmployeesByDepartment(selectedValue));

    }

    public void startUp(ApplicationIdentifier aIdentifier) throws EmployeeManagerControllerException
    {
        employeeView = EmployeeViewFactory.getView(aIdentifier);
        if (employeeView == null)
        {
            throw new EmployeeManagerControllerException(
                    "no view registered for application identifier '" + aIdentifier + "'!!");
        }
        employeeView.setController(this);
        employeeView.showView();
        employeeModel = EmployeeModelFactory.getModel(aIdentifier);
        if (employeeModel == null)
        {
            throw new EmployeeManagerControllerException(
                    "no model registered for application identifier '" + aIdentifier + "'!!");
        }
        employeeView.acceptCompanies(employeeModel.getCompanies());
    }

    @Override
    public void createCompany()
    {

    }

    public void refreshView()
    {
        employeeView.acceptCompanies(employeeModel.getCompanies());
    }
    
    public void refreshDepartmentView(Company aCompany)
    {
        employeeView.acceptDepartmens(employeeModel.getDepartmentsByCompany(aCompany));
    }

    @Override
    public void manageProcesses()
    {
        refreshView();
    }

    @Override
    public void deployProcess()
    {
        
    }

    @Override
    public void deleteCompany(Company aCompany)
    {
        employeeModel.deleteCompany(aCompany);
        refreshView();
    }

    @Override
    public void addDepartment(Company selectedCompany, String newDepartmentName)
    {
        employeeView.acceptDepartmens(employeeModel.addDepartment(selectedCompany, newDepartmentName));
        refreshDepartmentView(selectedCompany);
    }

    @Override
    public void deleteDepartment(Company selectedCompany, Department selectedDepartment)
    {
        employeeView.acceptDepartmens(employeeModel.deleteDepartment(selectedDepartment));
        refreshDepartmentView(selectedCompany);
    }
}