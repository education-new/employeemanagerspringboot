package de.sternico.employeemanager.client.util.http;

import de.sternico.employeemanager.server.controller.dto.CreateDepartment;
import de.sternico.employeemanager.server.controller.dto.CreateEmployee;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public class JsonifyObject
{
    public static void main(String[] args)
    {
        // CreateDepartment
        
        CreateDepartment aCreateDepartment = new CreateDepartment();
        
        aCreateDepartment.setCompanyName("Karstadt");
        
        Department aDepartment = new Department();
        aDepartment.setName("Einkauf");
        
        aCreateDepartment.setDepartment(aDepartment);
        
        System.out.println(new ObjectJsonifyer<CreateDepartment>().jsonify(aCreateDepartment));
        
        // Company
        
        Company aCompany = new Company();
        
        aCompany.setName("Karstadt");
        
        System.out.println(new ObjectJsonifyer<Company>().jsonify(aCompany));
        
        // Employee
        
        CreateEmployee aCreateEmployee = new CreateEmployee();
        
        aCreateEmployee.setCompanyName("Lufthansa");
        aCreateEmployee.setDepartmentName("Logistik");
        aCreateEmployee.setEmployeeFirstName("Max");
        aCreateEmployee.setEmployeeLastName("Mustermann");
        
        System.out.println(new ObjectJsonifyer<CreateEmployee>().jsonify(aCreateEmployee));
    }
}