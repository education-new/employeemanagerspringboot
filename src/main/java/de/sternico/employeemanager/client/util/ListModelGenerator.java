package de.sternico.employeemanager.client.util;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

public class ListModelGenerator<T>
{
    public ListModel<T> generate(List<T> aItems)
    {
        DefaultListModel<T> model = new DefaultListModel<T>();
        for (T item : aItems)
        {
            model.addElement(item);
        }
        return model;
    }
}