package de.sternico.employeemanager.client.util.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectJsonifyer<T>
{
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
    
    public String jsonify(T item)
    {
        try
        {
            return JSON_MAPPER.writeValueAsString(item);
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}