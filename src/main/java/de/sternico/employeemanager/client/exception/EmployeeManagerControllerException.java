package de.sternico.employeemanager.client.exception;

public class EmployeeManagerControllerException extends Exception
{
    private static final long serialVersionUID = -3699999532644471730L;
    
    public EmployeeManagerControllerException(String aMessage)
    {
        super(aMessage);
    }
}