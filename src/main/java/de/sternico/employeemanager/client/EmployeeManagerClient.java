package de.sternico.employeemanager.client;

import de.sternico.employeemanager.client.controller.EmployeeManagerClientController;
import de.sternico.employeemanager.client.exception.EmployeeManagerControllerException;

public class EmployeeManagerClient
{
    public static void main(String[] args)
    {
        try
        {
            new EmployeeManagerClientController().startUp(ApplicationIdentifier.SPRING);
        }
        catch (EmployeeManagerControllerException e)
        {
            e.printStackTrace();
        }
    }
}