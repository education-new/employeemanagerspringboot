package de.sternico.employeemanager.client.model;

import java.util.ArrayList;
import java.util.List;

import de.sternico.employeemanager.client.http.HttpAction;
import de.sternico.employeemanager.client.http.RestClient;
import de.sternico.employeemanager.client.model.base.EmployeeManagerModel;
import de.sternico.employeemanager.server.controller.dto.CreateDepartment;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public class SpringEmployeeManagerModel extends EmployeeManagerModel
{
    private static final String URL_GET_COMPANIES = "http://localhost:8080/companies";

    private static final String URL_GET_DEPARTMENTS = "http://localhost:8080/departments";
    private static final String URL_PUT_DEPARTMENT = "http://localhost:8080/department";
    private static final String URL_DELETE_DEPARTMENT = "http://localhost:8080/deleteDepartment";

    private static final String URL_GET_EMPLOYEES = "http://localhost:8080/employees";
    
    @SuppressWarnings(
    {
            "rawtypes", "unchecked"
    })
    @Override
    public List<Company> getCompanies()
    {
        try
        {
            return new RestClient().execute(URL_GET_COMPANIES, HttpAction.GET, Company.class, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @SuppressWarnings(
    {
            "unchecked", "rawtypes"
    })
    @Override
    public List<Department> getDepartmentsByCompany(Company aCompany)
    {
        try
        {
            return new RestClient().execute(URL_GET_DEPARTMENTS, HttpAction.POST, Department.class, aCompany);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @SuppressWarnings(
    {
            "unchecked", "rawtypes"
    })
    @Override
    public List<Employee> getEmployeesByDepartment(Department aDepartment)
    {
        try
        {
            return new RestClient().execute(URL_GET_EMPLOYEES, HttpAction.POST, Employee.class, aDepartment);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
    
    @SuppressWarnings({
            "unchecked", "rawtypes"
    })
    @Override
    public List<Department> addDepartment(Company selectedCompany, String newDepartmentName)
    {
        CreateDepartment aCreateDepartment = new CreateDepartment();
        aCreateDepartment.setCompanyName(selectedCompany.getName());

        Department aDepartment = new Department();
        aDepartment.setName(newDepartmentName);

        aCreateDepartment.setDepartment(aDepartment);
        
        try
        {
            return new RestClient().execute(URL_PUT_DEPARTMENT, HttpAction.PUT, Department.class, aCreateDepartment);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @SuppressWarnings({
            "unchecked", "rawtypes"
    })
    @Override
    public List<Department> deleteDepartment(Department selectedDepartment)
    {
        try
        {
            return new RestClient().execute(URL_DELETE_DEPARTMENT, HttpAction.DELETE, Department.class, selectedDepartment);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
    

    @Override
    public void deleteCompany(Company aCompany)
    {
        // TODO Auto-generated method stub
    }
}
