package de.sternico.employeemanager.client.model;

import java.util.HashMap;

import de.sternico.employeemanager.client.ApplicationIdentifier;
import de.sternico.employeemanager.client.model.base.EmployeeManagerModel;

public class EmployeeModelFactory
{
    private static final HashMap<ApplicationIdentifier, EmployeeManagerModel> models =
            new HashMap<ApplicationIdentifier, EmployeeManagerModel>();
    static
    {
        models.put(ApplicationIdentifier.SPRING, new SpringEmployeeManagerModel());
    }

    public static EmployeeManagerModel getModel(ApplicationIdentifier aIdentifier)
    {
        return models.get(aIdentifier);
    }
}