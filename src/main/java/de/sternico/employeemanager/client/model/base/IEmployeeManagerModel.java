package de.sternico.employeemanager.client.model.base;

import java.util.List;

import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public interface IEmployeeManagerModel
{
    List<Company> getCompanies();
    
    List<Department> getDepartmentsByCompany(Company aCompany);
    
    List<Employee> getEmployeesByDepartment(Department aDepartment);
    
    void deleteCompany(Company aCompany);

    List<Department> addDepartment(Company selectedCompany, String newDepartmentName);

    List<Department> deleteDepartment(Department selectedDepartment);
}