package de.sternico.employeemanager.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long>
{
    Department findByName(String name);
    
    List<Department> findByCompany(Company company);
}