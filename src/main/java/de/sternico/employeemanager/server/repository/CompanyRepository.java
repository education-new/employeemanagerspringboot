package de.sternico.employeemanager.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.sternico.employeemanager.server.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>
{

    Company findByName(String name);
}