package de.sternico.employeemanager.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>
{    
    List<Employee> findByDepartment(Department department);
}
