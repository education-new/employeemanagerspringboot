package de.sternico.employeemanager.server.delegate;

import org.apache.log4j.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.base.EmployeeManagerDelegate;

public class CreateEmployeeDelegate extends EmployeeManagerDelegate
{
    private static final Logger logger = Logger.getLogger(CreateEmployeeDelegate.class);

    

    @Override
    public void execute(DelegateExecution execution) throws Exception
    {        
        System.out.println(execution.getVariable(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME));
        System.out.println(execution.getVariable(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME));
        System.out.println(execution.getVariable(BpmnDefinitions.Variables.DEPARTMENT_NAME));
        System.out.println(execution.getVariable(BpmnDefinitions.Variables.COMPANY_NAME));
        
         //execution.setVariable(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE, false);
        
    }

}
