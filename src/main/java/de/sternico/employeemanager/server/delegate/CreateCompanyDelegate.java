package de.sternico.employeemanager.server.delegate;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.base.EmployeeManagerDelegate;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.repository.CompanyRepository;

@Named("createCompanyDelegate")
public class CreateCompanyDelegate extends EmployeeManagerDelegate
{
    private static final Logger logger = Logger.getLogger(CreateCompanyDelegate.class);

    @Autowired
    private CompanyRepository companyRepository;
    
    @Override
    public void execute(DelegateExecution execution) throws Exception
    {
        // will be null in a camunda test scenario!!
        if (companyRepository != null)
        {
            String companyName = (String) execution.getVariable(BpmnDefinitions.Variables.COMPANY_NAME);
            Company company = new Company();
            company.setName(companyName);
            companyRepository.save(company);
            System.out.println("created a company [" + companyName + "]...yeah!!!");
        }
    }
}