package de.sternico.employeemanager.server.delegate;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.base.EmployeeManagerDelegate;

@Named("checkProcessCountDelegate")
public class CheckProcessCountDelegate extends EmployeeManagerDelegate
{
    private static final Logger logger = Logger.getLogger(CheckProcessCountDelegate.class);
    
    @Override
    public void execute(DelegateExecution execution) throws Exception
    {
        execution.setVariable(BpmnDefinitions.Variables.TOO_MANY_COMPANIES_PENDING, false);
    }
}