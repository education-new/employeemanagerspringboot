package de.sternico.employeemanager.server.delegate;

import org.apache.log4j.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.base.EmployeeManagerDelegate;

public class CheckProcessCount extends EmployeeManagerDelegate
{

    private static final Logger logger = Logger.getLogger(CheckProcessCount.class);

    public int processCount;

    @Override
    public void execute(DelegateExecution execution) throws Exception
    {

        // String processCount = (String)
        // execution.getVariable(BpmnDefinitions.Variables.tooManyCompaniesPending);

        processCount = execution.getProcessEngine().getRuntimeService().createExecutionQuery().list().size();

        if (processCount >= 5)
        {
            execution.setVariable(BpmnDefinitions.Variables.TOO_MANY_COMPANIES_PENDING, true);

            // BpmnDefinitions.Variables.TOO_MANY_BOOL = (String)
            // execution.getVariable(BpmnDefinitions.Variables.TOO_MANY_COMPANIES_PENDING);

            System.out.println("TRUE");

        }
        else
        {

            execution.setVariable(BpmnDefinitions.Variables.TOO_MANY_COMPANIES_PENDING, false);
            System.out.println("FALSE");
        }

    }

}
