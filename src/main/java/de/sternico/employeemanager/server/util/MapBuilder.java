package de.sternico.employeemanager.server.util;

import java.util.HashMap;

public class MapBuilder
{
    private HashMap<String, Object> values = new HashMap<>();
    
    private MapBuilder()
    {
        super();
    }
    
    public static MapBuilder getInstance()
    {
        return new MapBuilder();
    }

    public MapBuilder addEntry(String aKey, Object aValue)
    {
        values.put(aKey, aValue);
        return this;
    }

    public HashMap<String, Object> create()
    {
        return values;
    }
}