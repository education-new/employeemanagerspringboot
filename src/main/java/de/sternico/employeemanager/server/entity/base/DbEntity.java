package de.sternico.employeemanager.server.entity.base;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DbEntity
{
    @Id
    @GeneratedValue
    private Long id;
}