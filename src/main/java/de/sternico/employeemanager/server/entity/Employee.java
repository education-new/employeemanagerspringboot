package de.sternico.employeemanager.server.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import de.sternico.employeemanager.server.entity.base.DbEntity;
import lombok.Data;

@Data
@Entity
public class Employee extends DbEntity implements Serializable
{
    private static final long serialVersionUID = -20940265965121195L;

    @ManyToOne
    @NotNull
    private Department department;
    
    @NotEmpty
    private String firstName;
    
    @NotEmpty
    private String lastName;
}