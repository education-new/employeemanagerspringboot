package de.sternico.employeemanager.server.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

import de.sternico.employeemanager.server.entity.base.DbEntity;
import lombok.Data;

@Data
@Entity
public class Company extends DbEntity implements Serializable
{
    private static final long serialVersionUID = -5921401423955110596L;
    
    @NotEmpty
    private String name;
}