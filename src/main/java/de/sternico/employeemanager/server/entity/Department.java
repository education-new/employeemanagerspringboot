package de.sternico.employeemanager.server.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import de.sternico.employeemanager.server.entity.base.DbEntity;
import lombok.Data;

@Data
@Entity
public class Department extends DbEntity implements Serializable
{
    private static final long serialVersionUID = -5379563117077073522L;

    @ManyToOne    
    @NotNull
    private Company company;
    
    @NotEmpty
    private String name;
}