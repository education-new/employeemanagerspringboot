package de.sternico.employeemanager.server.controller.dto;

import java.io.Serializable;

import de.sternico.employeemanager.server.entity.Employee;
import lombok.Data;

@Data
public class CreateEmployee implements Serializable
{
    private static final long serialVersionUID = -2953637485668335708L;

    private String companyName;
    private String departmentName;
    private String employeeFirstName;
    private String employeeLastName;
}
