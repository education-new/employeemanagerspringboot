package de.sternico.employeemanager.server.controller;

import java.util.List;

import javax.inject.Inject;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.controller.dto.CreateDepartment;
import de.sternico.employeemanager.server.controller.dto.CreateEmployee;
import de.sternico.employeemanager.server.entity.Company;
import de.sternico.employeemanager.server.entity.Department;
import de.sternico.employeemanager.server.entity.Employee;
import de.sternico.employeemanager.server.repository.CompanyRepository;
import de.sternico.employeemanager.server.repository.DepartmentRepository;
import de.sternico.employeemanager.server.repository.EmployeeRepository;
import de.sternico.employeemanager.server.util.MapBuilder;

@RestController
public class EmployeeManagerController
{
    private static final String TEST_COMPANY_NAME = "Lufthansa";

    private static final String TEST_DEPARTMENT1_NAME = "Einkauf";

    private static final String TEST_DEPARTMENT2_NAME = "Versand";

    private static final String TEST_EMPLOYEE1_LAST_NAME = "Hindenbung";

    private static final String TEST_EMPLOYEE2_LAST_NAME = "Müller";

    private static final String TEST_EMPLOYEE1_FIRST_NAME = "Frank";

    private static final String TEST_EMPLOYEE2_FIRST_NAME = "Luise";

    @Inject
    ProcessEngine processEngine;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/companies")
    public ResponseEntity<List<Company>> getCompanies()
    {
        return new ResponseEntity<List<Company>>(companyRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/departments")
    public ResponseEntity<List<Department>> getDepartments(@RequestBody Company aCompany)
    {
        Company company = companyRepository.findByName(aCompany.getName());
        return new ResponseEntity<List<Department>>(departmentRepository.findByCompany(company), HttpStatus.OK);
    }

    @PostMapping("/employees")
    public ResponseEntity<List<Employee>> getEmployees(@RequestBody Department aDepartment)
    {
        Department department = departmentRepository.findByName(aDepartment.getName());
        return new ResponseEntity<List<Employee>>(employeeRepository.findByDepartment(department), HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    @PutMapping("/employee")
    public ResponseEntity<String> createEmployee(@RequestBody CreateEmployee aEmployee)
    {
        Company aCompany = new Company();
        aCompany.setName(aEmployee.getCompanyName());
        
        Employee employee = new Employee();
        employee.setLastName(aEmployee.getEmployeeLastName());
        employee.setFirstName(aEmployee.getEmployeeFirstName());

        Department aDepartment = departmentRepository.findByName(aEmployee.getDepartmentName());

        if (aEmployee != null)
        {
            employee.setDepartment(aDepartment);
            employeeRepository.save(employee);
            return new ResponseEntity<String>("employee created.", HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<String>(
                    "employee NOT created --> department " + aEmployee.getDepartmentName() + " not found.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/department")
    public ResponseEntity<String> createDepartment(@RequestBody CreateDepartment aDepartment)
    {
        Department department = new Department();
        department.setName(aDepartment.getDepartment().getName());
        Company aCompany = companyRepository.findByName(aDepartment.getCompanyName());

        if (aCompany != null)
        {
            department.setCompany(aCompany);
            departmentRepository.save(department);
            return new ResponseEntity<String>("department created.", HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<String>(
                    "department NOT created --> company " + aDepartment.getCompanyName() + " not found.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/company")
    public ResponseEntity<String> createCompany(@RequestBody Company aCompany)
    {
        processEngine.getRuntimeService()
                .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.APPROVE_COMPANY_PROCESS,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, aCompany.getName())
                                .create());
        return new ResponseEntity<String>("create process for company " + aCompany.getName() + " started!!",
                HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    @DeleteMapping("/deleteDepartment")
    public ResponseEntity<String> deleteDepartment(@RequestBody Department aDepartment)
    {
        Department department = new Department();
        department.setName(aDepartment.getName());
        department.setCompany(aDepartment.getCompany());

        if (aDepartment != null)
        {
            departmentRepository.delete(department);
            return new ResponseEntity<String>("department deleted.", HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<String>(
                    "department NOT deleted --> department " + department.getName() + " not found.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // ------------------------------------------------------------------------------
    // --- test data creation
    // ------------------------------------------------------------------------------

    @PatchMapping("/testdata")
    public ResponseEntity<String> createTestData()
    {
        if (companyRepository.findAll().size() > 0)
        {
            return new ResponseEntity<String>("NO test data created --> data detected.", HttpStatus.OK);
        }

        // start a company process
        processEngine.getRuntimeService()
                .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.APPROVE_COMPANY_PROCESS,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, TEST_COMPANY_NAME)
                                .create());

        // approve the company
        Task approveCompanyTask = processEngine.getTaskService()
                .createTaskQuery()
                .taskDefinitionKey(BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY)
                .list()
                .get(0);
        processEngine.getTaskService()
                .complete(approveCompanyTask.getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.COMPANY_APPROVED, true).create());

        createDepartmentTestData(TEST_DEPARTMENT1_NAME);
        createDepartmentTestData(TEST_DEPARTMENT2_NAME);
        createEmployeeTestData(TEST_EMPLOYEE1_LAST_NAME, TEST_EMPLOYEE1_FIRST_NAME, TEST_DEPARTMENT1_NAME, TEST_COMPANY_NAME);
        createEmployeeTestData(TEST_EMPLOYEE2_LAST_NAME, TEST_EMPLOYEE2_FIRST_NAME, TEST_DEPARTMENT2_NAME, TEST_COMPANY_NAME);

        return new ResponseEntity<String>("test data created.", HttpStatus.OK);
    }

    private void createDepartmentTestData(String aDepartmentName)
    {
        CreateDepartment aCreateDepartment = new CreateDepartment();
        aCreateDepartment.setCompanyName(TEST_COMPANY_NAME);

        Department aDepartment = new Department();
        aDepartment.setName(aDepartmentName);

        aCreateDepartment.setDepartment(aDepartment);
        createDepartment(aCreateDepartment);
    }

    private void createEmployeeTestData(String aEmployeeLastName, String aEmployeeFirstName, String aDepartmentName, String aCompanyName)
    {
        CreateEmployee aCreateEmployee = new CreateEmployee();
        
        aCreateEmployee.setCompanyName(aCompanyName);
        aCreateEmployee.setDepartmentName(aDepartmentName);
        aCreateEmployee.setEmployeeFirstName(aEmployeeFirstName);
        aCreateEmployee.setEmployeeLastName(aEmployeeLastName);
        createEmployee(aCreateEmployee);
    }
}