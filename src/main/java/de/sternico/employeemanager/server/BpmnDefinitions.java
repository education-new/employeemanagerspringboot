package de.sternico.employeemanager.server;

public class BpmnDefinitions
{
    public class ProcessKeys
    {
        public static final String APPROVE_COMPANY_PROCESS = "ApproveCompanyProcess";
        public static final String EXTENDED_APPROVE_COMPANY_PROCESS = "ExtendedApproveCompanyProcess";
        public static final String COVER_LETTER_PROCESS = "CoverLetterProcess";
    }    
    
    public class Variables
    {
        public static final String COMPANY_APPROVED = "companyApproved";
        public static final String COMPANY_NAME = "companyName";
        public static final String TOO_MANY_COMPANIES_PENDING = "tooManyCompaniesPending"; 
        
        //EMPLOYEE APPLCATION PROCESS
        public static final String APPLICATION_COMPLETE = "applicationComplete";
        public static final String WORKER_DEMAND = "workerDemand";
        public static final String INTERVIEW_RESULT = "interviewResult";
        
        public static final String EMPLOYEE_FIRST_NAME = "employeeFirstName";
        public static final String EMPLOYEE_LAST_NAME = "employeeLastName"; 
        public static final String DEPARTMENT_NAME = "departmentName";
    }
    
    public class Events
    {

    }

    public class Messages
    {
        public static final String MSG_CREATE_COMPANY = "MSG_CREATE_COMPANY";
        
        //COVER LETTER PROCESS
        public static final String MSG_APPLICATION_MESSAGE = "MSG_APPLICATION_MESSAGE";
        public static final String MSG_TERMIN_ADOPTED = "MSG_TERMIN_ADOPTED";
        public static final String MSG_TERMIN_DECLINED = "MSG_TERMIN_DECLINED";
    }
    
    public class Tasks
    {
        public static final String TASK_APPROVE_COMPANY = "TASK_APPROVE_COMPANY";
        public static final String TASK_CREATE_COMPANY = "TASK_CREATE_COMPANY";
        public static final String GET_PROCESS_COUNT = "GET_PROCESS_COUNT";        
        
        //COVER LETTER PROCESS
            // USER TASK
        public static final String TASK_CHECK_APPLICATION_COMPLETE = "TASK_CHECK_APPLICATION_COMPLETE";
        public static final String TASK_CHECK_WORK_DEMAND = "TASK_CHECK_WORK_DEMAND";
        public static final String TASK_INVITE_CANDIDATE = "TASK_INVITE_CANDIDATE";
        public static final String TASK_JOB_INTERVIEW = "TASK_JOB_INTERVIEW";
        public static final String TASK_REJECT_CANDIDATE = "TASK_REJECT_CANDIDATE";
            // SERVICE TASK
        public static final String TASK_CREATE_EMPLOYEE = "TASK_CREATE_EMPLOYEE";

    }
    
    public class GateWays
    {
        
        public static final String INVITATION_ANSWER = "INVITATION_ANSWER";
        
    }
    
    public class Errors
    {
           
    }   
}