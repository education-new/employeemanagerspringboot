package de.sternico.employeemanager.server.listener;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;

import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.repository.CompanyRepository;
import de.sternico.employeemanager.server.repository.DepartmentRepository;
import de.sternico.employeemanager.server.util.MapBuilder;

public class ApplicationDataCompleteTaskListener implements TaskListener
{
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void notify(DelegateTask delegateTask)
    {
        /*
         * // Test DatenÜbergabe delegateTask.setVariables(
         * MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "Klaus").create());
         * delegateTask.setVariables( MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME,
         * "Himmler").create()); delegateTask.setVariables(
         * MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand").create());
         * delegateTask.setVariables( MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.COMPANY_NAME,
         * "Lufthansa").create());
         */

        String empFirstName = (String) delegateTask.getVariable(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME);
        String empLastName = (String) delegateTask.getVariable(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME);

        String departmentName = (String) delegateTask.getVariable(BpmnDefinitions.Variables.DEPARTMENT_NAME);
        String companyName = (String) delegateTask.getVariable(BpmnDefinitions.Variables.COMPANY_NAME);

        // Später noch einfügen, dass auf der DB nachgefragt wird ob es das Department und die Company überhaupt gibt
        // || departmentRepository.findByName(departmentName) == null
        // || companyRepository.findByName(companyName) == null

        if (empFirstName == null ||
                empFirstName.length() == 0 || empLastName == null || empLastName.length() == 0 ||
                departmentName == null || departmentName.length() == 0 || companyName == null ||
                companyName.length() == 0)
        {
            // Sag das die ApplicationComplete == false
            delegateTask.setVariables(
                    MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, false).create());
        }
        else
        {
            // Sag das die ApplicationComplete == true
            delegateTask.setVariables(
                    MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, true).create());
        }
    }
}