package de.sternico.employeemanager;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import de.sternico.employeemanager.base.EmployeeManagerTest;
import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.CreateCompanyDelegate;
import de.sternico.employeemanager.server.util.MapBuilder;

@RunWith(SpringRunner.class)
@Deployment(resources = "ApproveCompanyProcess.bpmn")
public class CompanyProcessTest extends EmployeeManagerTest
{
    @Before
    public void setup()
    {
        Mocks.register("createCompanyDelegate", new CreateCompanyDelegate());
    }

    @Rule
    @ClassRule
    public static ProcessEngineRule processEngine = TestCoverageProcessEngineRuleBuilder.create().build();

    // works if the test is marked as '@DataJpaTest', but not in the
    // process engine test context...
    // @Autowired
    // private CompanyRepository companyRepository;

    @Test
    public void testApproveCompany()
    {
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.APPROVE_COMPANY_PROCESS);

        
        
        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.COMPANY_APPROVED, true).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CREATE_COMPANY);
        assertThat(instance).isEnded();
    }

    @Test
    public void testDeclineCompany()
    {
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.APPROVE_COMPANY_PROCESS);
        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.COMPANY_APPROVED, false).create());
        assertThat(instance).hasNotPassed(BpmnDefinitions.Tasks.TASK_CREATE_COMPANY);
        assertThat(instance).isEnded();
    }

    @After
    public void teardown()
    {
        Mocks.reset();
    }
}