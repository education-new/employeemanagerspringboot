package de.sternico.employeemanager.base;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.ProcessEngineRule;

public abstract class EmployeeManagerTest
{
    protected static Task getTask(ProcessEngineRule processEngine,
            ProcessInstance processInstance, String taskDefinitionKey)
    {
        return processEngine.getTaskService()
                .createTaskQuery()
                .processInstanceId(processInstance.getId())
                .taskDefinitionKey(taskDefinitionKey)
                .list()
                .get(0);
    }
}