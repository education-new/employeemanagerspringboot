package de.sternico.employeemanager;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.sternico.employeemanager.base.EmployeeManagerTest;
import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.delegate.CheckProcessCountDelegate;
import de.sternico.employeemanager.server.util.MapBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@Deployment(resources = "ExtendedApproveCompanyProcess.bpmn")
public class ExtendedCompanyProcessTest extends EmployeeManagerTest
{
    @Before
    public void setup()
    {
        Mocks.register("checkProcessCountDelegate", new CheckProcessCountDelegate());
    }
    
    @Rule
    @ClassRule
    public static ProcessEngineRule processEngine = TestCoverageProcessEngineRuleBuilder.create().build();

    @Test
    public void testApproveCompany()
    {

        for (int i = 0; i <= 6; i++)
        {
            processEngine.getRuntimeService()
                    .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.EXTENDED_APPROVE_COMPANY_PROCESS);

        }

        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByKey(BpmnDefinitions.ProcessKeys.EXTENDED_APPROVE_COMPANY_PROCESS);

        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.GET_PROCESS_COUNT);

        if (instance.isEnded() == true)
        {
            System.out.println("JUHUUU ZU ENDE");

        }
        else
        {
            processEngine.getTaskService()
                    .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_APPROVE_COMPANY).getId(),
                            MapBuilder.getInstance()
                                    .addEntry(BpmnDefinitions.Variables.COMPANY_APPROVED, true)
                                    .create());
            assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CREATE_COMPANY);
            assertThat(instance).isEnded();
            System.out.println("TEST");
        }

    }
    
    @After
    public void teardown()
    {
        Mocks.reset();
    }
}