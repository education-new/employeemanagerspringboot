package de.sternico.employeemanager;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.qos.logback.core.net.server.Client;
import de.sternico.employeemanager.base.EmployeeManagerTest;
import de.sternico.employeemanager.server.BpmnDefinitions;
import de.sternico.employeemanager.server.util.MapBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@Deployment(resources = "EmployeeApplicationProcess.bpmn")
public class CreateEmployeeProcessTest extends EmployeeManagerTest
{

    @Rule
    @ClassRule
    public static ProcessEngineRule processEngine = TestCoverageProcessEngineRuleBuilder.create().build();

    @Test
    public void incompleteApplication_Test()
    {
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByMessage(BpmnDefinitions.Messages.MSG_APPLICATION_MESSAGE,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "")
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME, "Himmler")
                                .addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand")
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, "Lufthansa")
                                .create());

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);
        processEngine.getTaskService()
                .complete(
                        getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE).getId(),
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, false)
                                .create());

        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        assertThat(instance).isEnded();
    }

    @Test
    public void completeApplication_noDemand_Test()
    {
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByMessage(BpmnDefinitions.Messages.MSG_APPLICATION_MESSAGE,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "Klaus")
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME, "Himmler")
                                .addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand")
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, "Lufthansa")
                                .create());

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);
        processEngine.getTaskService()
                .complete(
                        getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE).getId(),
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, true)
                                .create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.WORKER_DEMAND, false).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        assertThat(instance).isEnded();
    }

    @Test
    public void completeApplication_Demand_Invitation_Declined_Test()
    {
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByMessage(BpmnDefinitions.Messages.MSG_APPLICATION_MESSAGE,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "Klaus")
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME, "Himmler")
                                .addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand")
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, "Lufthansa")
                                .create());

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);
        processEngine.getTaskService()
                .complete(
                        getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE).getId(),
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, true)
                                .create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.WORKER_DEMAND, true).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        assertThat(instance).isWaitingAt(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        sendMessage(runtimeService, BpmnDefinitions.Messages.MSG_TERMIN_DECLINED);
        assertThat(instance).hasPassed(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        assertThat(instance).isNotWaitingAt(BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        assertThat(instance).isEnded();
    }

    @Test
    public void completeApplication_Demand_Invitation_Adopted_interviewFailed_Test()
    {
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByMessage(BpmnDefinitions.Messages.MSG_APPLICATION_MESSAGE,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "Klaus")
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME, "Himmler")
                                .addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand")
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, "Lufthansa")
                                .create());

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);
        processEngine.getTaskService()
                .complete(
                        getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE).getId(),
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, true)
                                .create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.WORKER_DEMAND, true).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        assertThat(instance).isWaitingAt(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        sendMessage(runtimeService, BpmnDefinitions.Messages.MSG_TERMIN_ADOPTED);
        assertThat(instance).hasPassed(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        assertThat(instance).isNotWaitingAt(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.INTERVIEW_RESULT, false).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_REJECT_CANDIDATE);
        assertThat(instance).isEnded();
    }

    @Test
    public void completeApplication_Demand_Invitation_Adopted_interviewSuccess_Test()
    {
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByMessage(BpmnDefinitions.Messages.MSG_APPLICATION_MESSAGE,
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_FIRST_NAME, "Klaus")
                                .addEntry(BpmnDefinitions.Variables.EMPLOYEE_LAST_NAME, "Himmler")
                                .addEntry(BpmnDefinitions.Variables.DEPARTMENT_NAME, "Versand")
                                .addEntry(BpmnDefinitions.Variables.COMPANY_NAME, "Lufthansa")
                                .create());

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);
        processEngine.getTaskService()
                .complete(
                        getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE).getId(),
                        MapBuilder.getInstance()
                                .addEntry(BpmnDefinitions.Variables.APPLICATION_COMPLETE, true)
                                .create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_APPLICATION_COMPLETE);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.WORKER_DEMAND, true).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CHECK_WORK_DEMAND);

        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE).getId(),
                        MapBuilder.getInstance().create());

        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_INVITE_CANDIDATE);
        assertThat(instance).isWaitingAt(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        sendMessage(runtimeService, BpmnDefinitions.Messages.MSG_TERMIN_ADOPTED);
        assertThat(instance).hasPassed(BpmnDefinitions.GateWays.INVITATION_ANSWER);
        assertThat(instance).isWaitingAt(BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW);
        processEngine.getTaskService()
                .complete(getTask(processEngine, instance, BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW).getId(),
                        MapBuilder.getInstance().addEntry(BpmnDefinitions.Variables.INTERVIEW_RESULT, true).create());
        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_JOB_INTERVIEW);

        assertThat(instance).hasPassed(BpmnDefinitions.Tasks.TASK_CREATE_EMPLOYEE);
        assertThat(instance).isEnded();
    }

    private void sendMessage(RuntimeService runtimeService, String aMessageName)
    {
        runtimeService.correlateMessage(aMessageName);
    }

}
